<?php

	$array = json_decode(file_get_contents ("content/Script_News.json"));

	$count = 10;
	foreach($array as $meta){
		$count--;
		if($meta->formatting==="html"){
			echo '<div class="newsEvent">';
			echo $meta->title.'<div style="float:right;">'.$meta->date.'</div><br><hr>';
			echo file_get_contents("content/content/".$meta->link)."<br></div>";  
		}
	}
 ?>
<div id="box" class="box" style="display:none">
	<div id="box_main" >
		<div id="termin" style="width:100%;height:100%;">
			<div id="termin_Maphin" style="width:100%;">
					<b>
						IMP Termine
					</b>
					<table border="1" style="width:100%">
						<tr style="visibility:collapse">
							<td style="width:50%;">Datum</td>
							<td style="width:50%;"><a href="Termine.php">Inhalt</a></td>
						</tr>
					</table>
			</div>
			<div id="termin_Informatik" style="width:100%;">
					<b>
						Informatik Termine
					</b>
					<table border="1" style="width:100%">
						<tr style="visibility:collapse">
							<td style="width:50%;">Datum</td>
							<td style="width:50%;"><a href="https://fachschaft.informatik.hu-berlin.de/wiki/Hauptseite">Inhalt</a></td>
						</tr>
					</table>
			</div>
			<div id="termin_Mathe" style="width:100%;">
				<b>
					Mathe Termine
				</b>
				<table border="1" style="width:100%">
					<tr style="visibility:collapse">
						<td style="width:50%;">Datum</td>
						<td style="width:50%;"><a href="https://www.math.hu-berlin.de/~fsr/">Inhalt</a></td>
					</tr>
				</table>
			</div>
			<div id="termin_Physik" style="width:100%;">
				<b>
					Physik Termine
				</b>
				<table border="1" style="width:100%">
					<tr style="visibility:collapse">
						<td style="width:50%;">Datum</td>
						<td style="width:50%;"><a href="https://physik.hu/">Inhalt</a></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="js/kalender.js" charset="utf-8"></script>
<!-- oof -->