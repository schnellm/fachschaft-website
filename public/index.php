<!DOCTYPE html>
<!--
https://agnes.hu-berlin.de/lupo/rds?state=user&type=1&category=auth.login&re=last&startpage=portal.vm
-->
<html lang="de" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>IMP - Info,Mathe,Physik</title>

		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" type="image/png" href="favicon_32px.png" sizes="32x32">
		<link rel="icon" type="image/png" href="favicon_96px.png" sizes="96x96">
		<link rel="apple-touch-icon" sizes="180x180" href="favicon_180px.png">

		<link rel="stylesheet" href="css/index.css">
	</head>
	<body style="font-family:Calibri">
		<!--<noscript>
			<div id="nojs">
				Sie haben kein Javascript aktiviert!
			</div>
		</noscript>-->
		<div id="background_img2">
			<div id="background_img">
			</div>
		</div>
		<div id="header" class="header">
			<?php include 'Script_TaskbarIcons.php'; ?>
			<h3 id="fachinitext">
					Fachschaftsinitiative IMP HU Berlin <br>(Informatik,Physik und Mathe)
			</h3>
		</div>
		<a name="what_is_IMP">
		<div class="main" id="chunkHolder">
			<div class="chunk-l">
				<iframe class="chunk_video" width="560" height="315" src="https://www.youtube-nocookie.com/embed/XW3RZOdqOac" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="text">
					<b>IMP - Informatik, Mathe, Physik</b><br>
					Der neue Studiengang - jetzt nur im Handel für den einmaligen Preis von nur 4 Jahren ihres Lebens.<br>
					Bei IMP handelt es sich um einen Studiengang an der Humboldt-Universität zu Berlin.<br>
					Innerhalb von 4 Jahren (Regelstudienzeit) wird dir vieles über Informatik, Mathe und Physik beigebracht.<br>

					<br><b>Für wen ist dieser Studiengang geeignet?</b><br>
					Für Menschen die ein Interesse bzw. eine Passion für diese drei Themenfeldern haben.<br>
					Aber keine Sorge, ihr benötigt kein Vorwissen.<br>
					Das einzige was ihr benötigt ist Abitur - Sowie eine Durchschnittsnote von etwas besser als 2,0 (2020: NC von 1,5)<br>

	<!--        <ul>
<!            <li>Begruessungstext</li>
						<li>Was ist IMP?</li>
						<li>Wer sich für diesen Studiengang intressieren könnte</li>
						<li>Welche weiterfuehrende Optionen es gibt</li>
						<li>NC?</li>-

						<li>Stundenplan</li>
						<li>Standort informationen</li>
					</ul>-->
				</div>
			</div>
			<div class="chunk-r">
				<img src="img/preview.png" height="100%">
				<div class="text">
					<b> Schwerpunkt und Vertiefung</b><br>
					Du kannst später deinen Schwerpunkt in einem Thema setzen und dich darin vertiefen.<br>
					Falls du dich fragst: "Moment! Lerne ich hier etwa nur 3 Fächer sehr oberflächlich?", kann ich dich erleichtern:<br>
					In jedem Fach geht es nämlich richtig in die Tiefe.<br>
					<!-- Du kannst nähmlich nach dem Bachlor (dem Studium - nicht der Serie) einen Master in jedem <br>
					der drei Fächer an der HU machen,<br>
					Egal, welche Vertiefung oder welchen Schwerpunkt du gesetzt hast.<br> 
					fromHannes: nicht ganz sicher, ob das so stimmt
					--> 
					Ausserdem hat IMP kaum selbst eigene Module, wir klauen uns die Vorlesungen von den anderen.<br>
					Also Mathe mit den Mathematikern, Physik mit den Physikern und Informatik mit den Informatikern.<br>
					Das wird dich richtig ins Schwitzen bringen aber dafür mit einem tiefen Einblick belohnen! <br> 

				</div>
			</div>
			<div class="chunk-l">
					<img src="img/CMS_2015neu.png" height="315px" style="padding-left:50px">
				<div class="text" style="color:black;text-decoration: none;padding-left:50px">
					<b>Standort - Wohingenau?</b><br>
					Campus Adlershof!<br>
					Humboldt-Universität,<br>
					Rudower Chaussee 26,<br>
					12489 Berlin<br>
					<a href="https://www.hu-berlin.de/de/ueberblick/campus/adlershof/standardseite">
					 mehr Informationen.
					</a>
			</div>
		</div>
		</div>
		 <!--<a href="" target="_blank" rel="noopener noreferrer">-->
		<div class="chunk-r">
			<img src="img/preview.png" height="100%">
			<div class="text">
				<b> Checkliste:</b><br>
				Interessiert? Dann haben wir die passende Checkliste für dich:<br>
				&#9634;&nbsp;&nbsp;<a>Immatrikulieren</a><br>
				&#9634;&nbsp;&nbsp;<a>Stundenplan erstellen</a><br>
				&#9634;&nbsp;&nbsp;<a>Warm Up</a><br>
				&#9634;&nbsp;&nbsp;<a>Events zum Orientieren</a><br>
				&#9634;&nbsp;&nbsp;<a href="Freizeit.php#Fachschaftsfahrt">Fachschaftsfahrt anmelden</a><br>

			</div>
		</div>
	</body>
 
	<!--<script src="js/index_background.js" charset="utf-8"></script>-->
</html>
